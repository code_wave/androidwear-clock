package com.example.codewave.messagedisplay;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


public class DeviceActivity extends ActionBarActivity implements View.OnClickListener{

    Boolean red_selected = false, yellow_selected = false;

    Button next_button;

    ImageButton red_btn, yellow_btn;

    ImageView background_image_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Make the activity full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Set the layout
        setContentView(R.layout.activity_device);

        //Get references to the variables
        background_image_view = (ImageView)findViewById(R.id.background_image_view);

        red_btn = (ImageButton)findViewById(R.id.reb_background);

        yellow_btn = (ImageButton)findViewById(R.id.yellow_background);

        next_button = (Button)findViewById(R.id.next_background_button);

        //Set OnClickListeners to buttons
        red_btn.setOnClickListener(this);

        yellow_btn.setOnClickListener(this);

        next_button.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.device, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == red_btn.getId()){

            if(red_selected == false){
                red_selected = true;
                yellow_selected  = false;

                background_image_view.setBackgroundColor(getResources().getColor(R.color.RED));

            }

        }

        if(v.getId() == yellow_btn.getId()){

            if(yellow_selected == false){
                red_selected = false;
                yellow_selected  = true;

                background_image_view.setBackgroundColor(getResources().getColor(R.color.YELLOW));

            }

        }

        if(v.getId() == next_button.getId()){

            int selected;

            if(red_selected == true){
                selected = 1;
            }else if(yellow_selected == true){
                selected = 2;
            }else{
                Toast.makeText(getApplication(), "Select background for your watch", Toast.LENGTH_LONG).show();
                return;
            }

            // Take user to the new activity to select the watch dial
            Intent i = new Intent(DeviceActivity.this, WatchDialActivity.class);
            i.putExtra("SelectedCase", selected);
            startActivity(i);

        }

    }
}
