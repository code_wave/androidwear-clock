package com.example.codewave.messagedisplay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by codewave on 8/22/14.
 */
public class WatchDialActivity  extends Activity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        NodeApi.NodeListener{

    ImageButton first_hand, second_hand, third_hand, forth_hand;

    Boolean first_hand_selected = false, second_hand_selected = false, third_hand_selected = false, forth_hand_selected = false;

    Button save;

    GoogleApiClient googleClient;

    String MESSAGE_RECEIVED_PATH = "100";

    int send_case_no;

    ImageView hand_background_view, hmHand_view;

    Boolean isConnectedToWatch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Make the activity ful screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Set the layout
        setContentView(R.layout.watch_dial_layout);

        //Get the references to variables
        hand_background_view = (ImageView)findViewById(R.id.hand_background_view);

        hmHand_view = (ImageView)findViewById(R.id.hmHand_view);

        first_hand = (ImageButton)findViewById(R.id.first_hand_design);

        second_hand = (ImageButton)findViewById(R.id.second_hand_design);

        third_hand = (ImageButton)findViewById(R.id.third_hand_design);

        forth_hand = (ImageButton)findViewById(R.id.forth_hand_design);

        save = (Button)findViewById(R.id.save_button);

        //Get the Intent message from the previous activity - the case selected by the user and change the background accordingly
        Intent i = getIntent();
        int selected = i.getIntExtra("SelectedCase", 0);

        if(selected != 0){

            if(selected == 1){
                // Set the background here to red
                hand_background_view.setBackgroundColor(getResources().getColor(R.color.RED));
            }else if(selected == 2){
                // Set the background to yellow here
                hand_background_view.setBackgroundColor(getResources().getColor(R.color.YELLOW));
            }

        }

        //Set the OnclickListener to the buttons
        first_hand.setOnClickListener(this);

        second_hand.setOnClickListener(this);

        third_hand.setOnClickListener(this);

        forth_hand.setOnClickListener(this);

        save.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == first_hand.getId()){

            if(first_hand_selected == false){
                first_hand_selected = true;
                second_hand_selected = false;
                third_hand_selected = false;
                forth_hand_selected = false;

                hmHand_view.setBackgroundResource(R.drawable.clock_lightblue_new);

            }

        }

        if(v.getId() == second_hand.getId()){
            first_hand_selected = false;
            second_hand_selected = true;
            third_hand_selected = false;
            forth_hand_selected = false;

            hmHand_view.setBackgroundResource(R.drawable.clock_darkblue_new);

        }

        if(v.getId() == third_hand.getId()){
            first_hand_selected = false;
            second_hand_selected = false;
            third_hand_selected = true;
            forth_hand_selected = false;

            hmHand_view.setBackgroundResource(R.drawable.clock_grey_new);

        }

        if(v.getId() == forth_hand.getId()){
            first_hand_selected = false;
            second_hand_selected = false;
            third_hand_selected = false;
            forth_hand_selected = true;

            hmHand_view.setBackgroundResource(R.drawable.clock_yellow_new);

        }

        if(v.getId() == save.getId()){

            int sel = getIntent().getIntExtra("SelectedCase", 0);
            int case_no = 0;

            if(first_hand_selected == true){
                if(sel == 1){
                    case_no = 1;
                    show_dialog(case_no);
                }else if(sel == 2){
                    case_no = 2;
                    show_dialog(case_no);
                }
            }else if(second_hand_selected == true){
                if(sel == 1){
                    case_no = 3;
                    show_dialog(case_no);
                }else if(sel == 2){
                    case_no = 4;
                    show_dialog(case_no);
                }
            }else if(third_hand_selected == true){
                if(sel == 1){
                    case_no = 5;
                    show_dialog(case_no);
                }else if(sel == 2){
                    case_no = 6;
                    show_dialog(case_no);
                }
            }else if (forth_hand_selected == true){
                if(sel == 1){
                    case_no = 7;
                    show_dialog(case_no);
                }else if(sel == 2){
                    case_no = 8;
                    show_dialog(case_no);
                }
            }else{
                Toast.makeText(getApplicationContext(), "Select the H/M hand design for your watch", Toast.LENGTH_LONG).show();
            }
        }

    }

    void show_dialog(int case_num){



            send_case_no = case_num;

            AlertDialog.Builder alert = new AlertDialog.Builder(WatchDialActivity.this);
            LayoutInflater inflater = WatchDialActivity.this.getLayoutInflater();
            View layout = inflater.inflate(R.layout.dialog_for_watchface, null);
            alert.setView(layout);

            TextView t = (TextView)layout.findViewById(R.id.dialog_textview);
            t.setText("Apply the WatchFace to your watch ??");

            alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    connect();

                }
            });

            alert.show();





    }

    void connect(){
        // Build a new GoogleApiClient
        googleClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleClient.connect();
    }
    @Override
    protected void onStart() {
        super.onStart();
    }


    // Send a message when the data layer connection is successful.
    @Override
    public void onConnected(Bundle connectionHint) {


//        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
        sendDataLayerMessage();

    }

    // Disconnect from the data layer when the Activity stops
    @Override
    protected void onStop() {
        if (null != googleClient && googleClient.isConnected()) {
            googleClient.disconnect();
        }
        super.onStop();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    private void sendDataLayerMessage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Get the connected nodes and wait for results
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
                for (Node node : nodes.getNodes()) {
                    // Send a message and wait for result
                    MessageApi.SendMessageResult result =
                            Wearable.MessageApi.sendMessage(googleClient, node.getId(),
                                    MESSAGE_RECEIVED_PATH, Integer.toString(send_case_no).getBytes()).await();
                    if (result.getStatus().isSuccess()) {
                        Log.v("myTag", "Message sent to : " + node.getDisplayName());
                    }
                    else {
                        // Log an error
                        Log.v("myTag", "MESSAGE ERROR: failed to send Message");
                    }
                }
            }
        }).start();
    }

    @Override
    public void onPeerConnected(Node node) {

        isConnectedToWatch = true;

    }

    @Override
    public void onPeerDisconnected(Node node) {

        isConnectedToWatch = false;

    }

}
