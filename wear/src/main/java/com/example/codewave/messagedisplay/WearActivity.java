package com.example.codewave.messagedisplay;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.AnalogClock;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WearActivity extends Activity {

    private TextView mTextView;
    private ImageView mImageView;
    private String message = "null";
    private AnalogClock analogClock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mImageView = (ImageView) stub.findViewById(R.id.watch_background);
                analogClock = (AnalogClock) stub.findViewById(R.id.analog_clock);
                // Register a local broadcast receiver, defined is Step 3.
                IntentFilter messageFilter = new IntentFilter("message-forwarded-from-data-layer");
                MessageReceiver messageReceiver= new MessageReceiver();
                LocalBroadcastManager.getInstance(getApplication()).registerReceiver(messageReceiver, messageFilter);

            }
        });
    }

    public class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            message = intent.getStringExtra("message");
            Log.d("Log", message);
            // Display message in UI
            mTextView.setText(message);
            if("1".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.RED));

                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.first_case_layout, null));

            }else if("2".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.second_case_layout, null));
            }else if("3".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.third_case_layout, null));
            }else if("4".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.forth_case_layout, null));
            }else if("5".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.fifth_case_layout, null));
            }else if("6".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.sixth_case_layout, null));
            }else if("7".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.seventh_case_layout, null));
            }else if("8".equalsIgnoreCase(message)){
                mImageView.setBackgroundColor(getResources().getColor(R.color.YELLOW));
                RelativeLayout submitScoreLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
                // Create new LayoutInflater - this has to be done this way, as you can't directly inflate an XML without creating an inflater object first
                LayoutInflater inflater = getLayoutInflater();
                submitScoreLayout.addView(inflater.inflate(R.layout.eighth_case_layout, null));
            }
        }
    }


//    @Override
//    protected void onPause() {
//        super.onPause();
//        mImageView.setImageDrawable( null );
////        mClock.setTextColor( getResources().getColor( android.R.color.white ) );
////        mContainer.setBackgroundColor( getResources().getColor( android.R.color.black ) );
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//
//        if( message.equals("Hello") ) {
//
//            mImageView.setBackgroundResource(R.drawable.bananaslugs_logo);
//
//        } else {
//            mImageView.setImageResource( R.drawable.ic_launcher );
////            mClock.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
////            mContainer.setBackgroundColor(getResources().getColor( android.R.color.white ) );
//        }
//    }


}
